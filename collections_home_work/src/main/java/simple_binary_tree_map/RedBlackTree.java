package simple_binary_tree_map;


import java.util.Iterator;

public class RedBlackTree<T extends Comparable<T>>  {

    private Node _root;
    private Node _nil;

    public RedBlackTree() {
        _root = new Node();
        _nil = new Node();
        _nil._color = NodeColor.BLACK;
        _root._parent = _nil;
        _root._right = _nil;
        _root._left = _nil;
    }

    public void add(T o) {
        Node node = _root, temp = _nil;
        Node newNode = new Node((T) o, NodeColor.RED);
        while (node != null && node != _nil && !node.isFree()) {
            temp = node;
            if (newNode.getValue().compareTo(node.getValue()) < 0)
                node = node.getLeft();
            else
                node = node.getRight();
        }
        newNode.setParent(temp);
        if (temp == _nil)
            _root.setValue(newNode.getValue());
        else {
            if (newNode.getValue().compareTo(temp.getValue()) < 0)
                temp.setLeft(newNode);
            else
                temp.setRight(newNode);
        }
        newNode.setLeft(_nil);
        newNode.setRight(_nil);
    }

    public boolean remove(T o) {
        return remove((T) findNode(o));
    }

    public boolean contains(T o) {
        return (findNode(o) != _nil);
    }

    private Node findNode(T o) {
        Node node = _root;
        while (node != null && node != _nil && node.getValue().compareTo(o) != 0) {
            if (node.getValue().compareTo(o) > 0)
                node = node.getLeft();
            else
                node = node.getRight();
        }
        return node;
    }


    enum NodeColor {
        RED,
        BLACK,
        NONE
    }

    public class Node {

        private T _value;
        private NodeColor _color;
        private Node _parent;
        private Node _left;
        private Node _right;


        public Node() {
            _value = null;
            _color = NodeColor.NONE;
            _parent = null;
            _left = null;
            _right = null;
        }

        public Node(T value, NodeColor color) {
            _value = value;
            _color = color;
            _parent = _nil;
            _left = _nil;
            _right = _nil;
        }


        public boolean isFree() {
            return _value == null || _value == _nil;
        }

        public T getValue() {
            return _value;
        }

        public void setValue(T value) {
            _value = value;
        }


        public void setParent(Node node) {
            _parent = node;
        }

        public Node getLeft() {
            return _left;
        }

        public void setLeft(Node node) {
            _left = node;
            if (_left != null && _left != _nil) _left._parent = this;
        }

        public Node getRight() {
            return _right;
        }

        public void setRight(Node node) {
            _right = node;
            if (_right != null && _right != _nil) _right._parent = this;
        }

    }

}