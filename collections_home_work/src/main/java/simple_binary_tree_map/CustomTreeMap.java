package simple_binary_tree_map;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class CustomTreeMap<K extends Comparable, V> {

    private Set<K> key;
    private List<V> value;

    public CustomTreeMap() {
        key = new TreeSet<>();
        value = new ArrayList<>();
    }

    public int size(){
        return key.size();
    }

    public boolean isEmpty(){
        return key.size() == 0;
    }

    public void clear(){
        key = new TreeSet<>();
        value = new ArrayList<>();
    }

    public V put(K key, V value){
        this.key.add(key);
        this.value.add(getIndex(key), value);
        return value;
    }

    public V get(K key){
        int position = getIndex(key);
        if(position<0){
            return null;
        }else {
            return value.get(position);
        }
    }

    public V remove(K key){
        int position = getIndex(key);
        if(position<0){
            return null;
        }else {
            V value= get(key);
            this.value.remove(value);
            this.key.remove(key);
            return value;
        }
    }

    public String print(){
        StringBuilder s = new StringBuilder();
        for (K k : key) {
            s.append("(").append(k.toString()).append(", ")
                    .append(get(k).toString()).append(" )");
        }
        return s.toString();
    }

    private int getIndex(K key){
        int position=0;
        for (K k : this.key) {
            if (k.equals(key)){
                break;
            }
            position++;
        }
        if(position>= this.key.size()){
            position=-1;
        }
        return position;
    }


}

