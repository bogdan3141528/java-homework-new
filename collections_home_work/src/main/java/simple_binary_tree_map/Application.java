package simple_binary_tree_map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.TreeMap;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        CustomTreeMap<Integer, String> list = new CustomTreeMap<>();
        TreeMap<Integer,String> list1 = new TreeMap<>();
        list.put(4, "First");
        list.put(10,"Second");
        list.put(15,"uygkygyugkuykgugjkg");
        list.put(5, "AA");
        list.put(6, "BBB");
        list.put(6, "T");
        list.put(13, "F");
        list.put(1,"dasdasdasdasd");

        list1.put(4, "First");
        list1.put(10,"Second");
        list1.put(15,"uygkygyugkuykgugjkg");
        list1.put(5, "AA");
        list1.put(6, "BBB");
        list1.put(6, "T");
        list1.put(13, "F");
        list1.put(1,"dasdasdasdasd");

        logger.info(list.print());
        logger.info(list.get(10));
        System.out.println("Initial Mappings are: " + list1);



     /*   logger.info(list.remove(2));
        logger.info(list.print());
        logger.info(list.remove(3));
        logger.info(list.print());
        logger.info(list.remove(4));
        logger.info(list.print());
        logger.info(list.size());*/
    }
}
