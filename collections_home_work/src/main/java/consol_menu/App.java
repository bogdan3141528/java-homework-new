package consol_menu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class App {
    private Logger logger = LogManager.getLogger(App.class);
    private int c;


    App() {
        Menu mainMenu = new Menu("Main", "main menu");
        final Menu subMenuAdd = new Menu("Add", "add menu");
        Menu subMenuSubtraction = new Menu("Subtraction", "subtraction menu");

        mainMenu.putAction("Add",()->activateMenu(subMenuAdd));
        mainMenu.putAction("Subtraction",()->activateMenu(subMenuSubtraction));
        mainMenu.putAction("quit", () -> System.exit(0));

        subMenuAdd.putAction("add", this::add);
        subMenuAdd.putAction("main menu",()->activateMenu(mainMenu));
        subMenuAdd.putAction("quit", () -> System.exit(0));


        subMenuSubtraction.putAction("subtraction", this::subtraction);
        subMenuSubtraction.putAction("main menu",()->activateMenu(mainMenu));
        subMenuSubtraction.putAction("quit", () -> System.exit(0));

        activateMenu(mainMenu);
    }

    private void activateMenu(Menu newMenu) {
        logger.info(newMenu.generateText());
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int actionNumber = scanner.nextInt();
            newMenu.executeAction(actionNumber);
        }
    }

    private void add() {
        Scanner scanner = new Scanner(System.in);
        logger.info("Enter the first number");
        int a = scanner.nextInt();
        logger.info("Enter the second number");
        int b = scanner.nextInt();
        c = a + b;
        logger.info(a + "+" + b + "=" + c);
    }

    private void subtraction() {
        Scanner scanner = new Scanner(System.in);
        logger.info("Enter the first number");
        int a = scanner.nextInt();
        logger.info("Enter the second number");
        int b = scanner.nextInt();
        c = a - b;
        logger.info(a + "-" + b + "=" + c);
    }

}


