package stream_method;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.List;

public class MainStreamMethod {

    private static final Logger logger = LogManager.getLogger(MainStreamMethod.class);

    public static void main(String[] args) {
        List<Integer> list;
        StreamMethods streamMethods = new StreamMethods();
        list = streamMethods.generateRandomArray();
        logger.info(list);
        logger.info("SUM = "+streamMethods.getSum(list));
        logger.info("SUM Using reduce = "+streamMethods.getReduceSum(list));
        logger.info("AVERAGE = "+streamMethods.getAverage(list));
        logger.info("MAX = "+streamMethods.getMax(list));
        logger.info("MIN = "+streamMethods.getMin(list));
        logger.info("Sum values bigger then average = "+streamMethods.getSumValuesBiggerThanAverage(list));
    }
}
