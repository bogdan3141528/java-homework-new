package stream_method;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


class StreamMethods {

    List<Integer> generateRandomArray(){
        ArrayList<Integer> list;
        Random random = new Random();
        list = IntStream.range(0, 10).mapToObj(i -> random.nextInt(1000)).collect(Collectors.toCollection(() -> new ArrayList<>(10)));
        return list;
    }

    double getAverage(List<Integer> list){
        return list.stream().mapToInt(Integer::intValue).average().getAsDouble();
    }

    int  getSum(List<Integer> list){
        return list.stream().mapToInt(Integer::intValue).sum();
    }

    int getMin(List<Integer> list){
        return list.stream().mapToInt(Integer::intValue).min().getAsInt();
    }

    int getMax(List<Integer> list){
        return list.stream().mapToInt(Integer::intValue).max().getAsInt();
    }

     int getReduceSum(List<Integer> list){
        return list.stream().mapToInt(Integer::intValue).reduce(Integer::sum).getAsInt();
    }

    Integer getSumValuesBiggerThanAverage(List<Integer> list){ 
        return list.stream().filter( v ->  v > getAverage(list)).reduce(Integer::sum).get();
    }

}
