package stream_app;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class MainStreamApp {

    private static final Logger logger = LogManager.getLogger(MainStreamApp.class);

    public static void main(String[] args) {
        StreamMethods streamMethods = new StreamMethods();
        List<String> list;
        logger.info(list = streamMethods.readingText());
        logger.info("Count unique word = " + streamMethods.uniqueWordCount(list));
        logger.info("Sort uniqueWord ");
        streamMethods.sortUniqueWord(list);
        logger.info("Occurrence number of each word in the text ");
        streamMethods.occurrenceNumberOdInTheText(list);
        logger.info(streamMethods.occurrenceNumberOfEachSymbolExceptUpperCaseCharacters(list));

    }
}
