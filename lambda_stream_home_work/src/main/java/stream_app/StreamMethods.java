package stream_app;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class StreamMethods {
    private static final Logger logger = LogManager.getLogger(StreamMethods.class);
    private List<String> list = new ArrayList<>();

    List<String> readingText(){
        Scanner sc = new Scanner(System.in);
        logger.info("Please insert text:");
        String line;
        while (!(line=sc.nextLine()).equals("")){
            String[] a = line.split(" ");
            Collections.addAll(list, a);
        }
        return list;
    }

    private Stream<Map.Entry<String, Long>> uniqueStream(List<String> list) {
        return list.stream().map(String::toLowerCase)
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
                .entrySet().stream()
                .filter(e -> e.getValue() == 1);
    }

    long uniqueWordCount(List<String> list) {
        return uniqueStream(list)
                .count();
    }

    void sortUniqueWord(List<String> list) {
        uniqueStream(list)
                .forEach(i -> logger.info(i.getKey()));
    }

    void occurrenceNumberOdInTheText(List<String> list) {
        list.stream().map(String::toLowerCase)
                .collect(Collectors.groupingBy(w -> w, Collectors.counting()))
                .entrySet()
                .forEach(logger::info);
    }

    Map<Character, Long> occurrenceNumberOfEachSymbolExceptUpperCaseCharacters(List<String> list) {
        String s = String.join("", list);
        List<Character> chars = s.chars().mapToObj(e -> (char) e)
                .collect(Collectors.toList());
        List<Character> charsLowerCaseOnly = chars.stream().
                filter(Character::isLowerCase).collect(Collectors.toList());
        return charsLowerCaseOnly.stream()
                .collect(Collectors.groupingBy(symbol -> symbol, Collectors.counting()));
    }

}