package functionalinterface;

@FunctionalInterface
public interface IntegerFunctionalInterface {

    int doActions(int a, int b, int c);

}
