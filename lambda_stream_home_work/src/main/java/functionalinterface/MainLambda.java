package functionalinterface;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainLambda {

    private static final Logger logger = LogManager.getLogger(MainLambda.class);

    public static void main(String[] args) {
        IntegerFunctionalInterface max = (a,b,c) -> {
            if ((a > b) && (a > c)) {
                return a;
            } else if ((b > a) && (b > c)) {
                return b;
            } else {
                return c;
            }
        };
        IntegerFunctionalInterface average = (a,b,c)->(a + b + c)/3;
        logger.info(max.doActions(5,10,18));
        logger.info(average.doActions(10,20,30));

    }
}
