package command_in_console.finterface;

@FunctionalInterface
public interface Command {

    void print (String m);

}
