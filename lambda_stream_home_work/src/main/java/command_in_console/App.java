package command_in_console;

import command_in_console.finterface.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class App {
    private static Logger logger = LogManager.getLogger(App.class);
    private Command command = m -> logger.info("Object"+m);

    public App() {
        Menu mainMenu = new Menu("Main", "main menu");

        mainMenu.putAction("Lambda Command",m-> logger.info("lambda"+m));
        mainMenu.putAction("Method reference Command", App::print);
        mainMenu.putAction("Anonymous Class  Command", new Command() {
                    @Override
                    public void print(String m) {
                        logger.info("Anonymous class"+m); }});
        mainMenu.putAction("Object Command",command);
        mainMenu.putAction("quit", m -> System.exit(0));

        activateMenu(mainMenu);
    }

    private static void print(String m) {
        logger.info("Method reference" + m);
    }

    private void activateMenu(Menu newMenu) {
        logger.info(newMenu.generateText());
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int actionNumber = scanner.nextInt();
            newMenu.executeAction(actionNumber); }
    }

}
