package command_in_console;

import command_in_console.finterface.Command;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.IntStream;

public class Menu  {
    private static Logger logger = LogManager.getLogger(Menu.class);
    private final String name;
    private final String text;
    private LinkedHashMap<String, Command> actionsMap = new LinkedHashMap<String, Command>();

    public Menu(String name, String text) {
        this.name = name;
        this.text = text;
    }

    public void putAction(String name, Command command) {
        actionsMap.put(name, command);
    }

    public String generateText() {
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(": ");
        sb.append(text).append(":\n");
        List<String> actionNames = new ArrayList<String>(actionsMap.keySet());
        IntStream.range(0, actionNames.size())
                .mapToObj(i -> String.format(" %d: %s%n", i + 1, actionNames.get(i)))
                .forEach(sb::append);
        return sb.toString();
    }

    public void executeAction(int actionNumber) {
        int effectiveActionNumber = actionNumber - 1;
        if (effectiveActionNumber < 0 || effectiveActionNumber >= actionsMap.size()) {
            System.out.println("Ignoring menu choice: " + actionNumber);
        } else {
            List<Command> actions = new ArrayList<Command>(actionsMap.values());
            actions.get(effectiveActionNumber).print("!!!!!!");
        }
    }
    void print (String m){
        logger.info("Object"+m);
    }
}
