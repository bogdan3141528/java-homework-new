package string_in_to_integer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ship_with_droids.MainGenericClassShipWithDroid;

import java.util.ArrayList;
import java.util.List;

public class MainStringInToInteger {
    private static Logger log = LogManager.getLogger(MainStringInToInteger.class);

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<Integer>();
        list.add(2);
        list.add(3);
        list.add(4);

        Inserter inserter = new Inserter();
        inserter.insertValue(list);
        log.info(list);
        log.info(list.get(3).getClass());
    }
}
