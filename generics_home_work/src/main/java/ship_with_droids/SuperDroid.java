package ship_with_droids;

public class SuperDroid {

    private String name;

    public SuperDroid(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        return "name=" + name;
    }
}
