package ship_with_droids;

import java.util.ArrayList;
import java.util.List;
import java.util.Collection;

public class Ship<T> {

    /*private T droid;
    public void loadDroid(T droid){
        this.droid = droid;
    }
    public T unloadDroid(){
        return this.droid;
    }
    public  <T extends SuperDroid> void putDroid(List<? super SuperDroid> list,T droid ){
        list.add(droid);
    }*/
    private List<T> list;

    public Ship() {
        this.list = new ArrayList<>();
    }

    public List<T> getDroid() {
        return list;
    }

    public void setDroid(T t) {
        this.list.add(t);
    }


    public void setAll(Collection <?extends T> collection){
        for (T t: collection) {
            list.add(t);
        }
    }

    @Override
    public String toString() {
        return "Ship{" +
                "list=" + list +
                '}';
    }


}
