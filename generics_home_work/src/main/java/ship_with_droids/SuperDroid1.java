package ship_with_droids;

public class SuperDroid1 extends Droid1 implements Comparable<SuperDroid1> {
    private int power;
    private int armor;

    public SuperDroid1(String name, int power, int armor) {
        super(name);
        this.power = power;
        this.armor = armor;
    }

    @Override
    public String toString() {
        return "SuperDroid{" + "name=" + getName() +
                ", power=" + power +
                ", armor=" + armor +
                '}';
    }



    @Override
    public int compareTo(SuperDroid1 o) {
        return this.getName().compareTo(o.getName());
    }
}
