package ship_with_droids;

public class Droid1 {
    private String name;

    public Droid1(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Droid1{" +
                "name='" + name + '\'' +
                '}';
    }
}
