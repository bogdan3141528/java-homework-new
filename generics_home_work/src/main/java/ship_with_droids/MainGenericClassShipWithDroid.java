package ship_with_droids;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class MainGenericClassShipWithDroid {
    private static Logger log = LogManager.getLogger(MainGenericClassShipWithDroid.class);

    public static void main(String[] args) {
      /*  Droid droid = new Droid("Droid1");
        SuperDroid superdroid = new SuperDroid("SuperDroid1");

        Ship shipRAW = new Ship();
        shipRAW.loadDroid(droid);
        log.info(shipRAW.unloadDroid());
        shipRAW.loadDroid(superdroid);
        log.info(shipRAW.unloadDroid());

        Ship<Droid> shipDeclaredType = new Ship<Droid>();
        //shipDeclaredType.loadDroid(superdroid); //not compile
        shipDeclaredType.loadDroid(droid);
        log.info(shipDeclaredType.unloadDroid());

        List<Droid> droids = new ArrayList<Droid>();
        List<SuperDroid> superDroids = new ArrayList<SuperDroid>();

        shipRAW.putDroid(droids,droid);
        shipRAW.putDroid(superDroids,superdroid);
        shipRAW.putDroid(superDroids,droid);
        shipRAW.putDroid(droids,superdroid); // compile
        droids.forEach(shipRaW -> log.info(droid));// not compile class cast exception

        //shipDeclaredType.putDroid(droids,droid);//not compile
       // shipDeclaredType.putDroid(droids,superdroid);//not compile
        shipDeclaredType.putDroid(superDroids,superdroid);
        shipDeclaredType.putDroid(superDroids,droid);*/


        Ship<Droid1> ship = new Ship();
        ship.setDroid(new Droid1("Desert"));
        ship.setDroid(new Droid1("Kalag"));
        ship.setDroid(new SuperDroid1("dsad",15,15));
        ship.setDroid(new ModernDroid("dsadasd",123));
        log.info(ship.getDroid());


        List<ModernDroid> modernDroidList = new ArrayList<>();
        modernDroidList.add(new ModernDroid("ferr",55));
        modernDroidList.add(new ModernDroid("feree",50));

        List<SuperDroid1> superDroidList = new ArrayList<>();
        superDroidList.add(new SuperDroid1("ferr",55,10));
        superDroidList.add(new SuperDroid1("feree",50, 12));

        ship.setAll(modernDroidList);
        ship.setAll(superDroidList);
        log.info(ship.getDroid());

    }
}
