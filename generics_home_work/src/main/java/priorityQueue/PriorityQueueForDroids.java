package priorityQueue;


public class PriorityQueueForDroids<E> {

    private E [] array;
    PriorityQueueForDroids() {
        array = (E[]) new Object[0];
    }

    public void add(E item) {
        if (array.length == 0) {
            array = (E[]) new Object[1];
            array[0] = item; }
        else {
            Object[] newItems = new Object[array.length + 1];
            newItems[newItems.length - 1] = item;
            for (int i = 0; i < newItems.length - 1; i++) {
                newItems[i] = array[i]; }
            array = (E[]) newItems; }
    }

    public void addFirst(E item) {
        if (array.length == 0) {
            array = (E[]) new Object[1];
            array[0] = item; }
        else {
            Object[] newArray = new Object[array.length + 1];
            newArray[0] = item;
            for (int i = 1; i < newArray.length; i++) {
                newArray[i] = array[i - 1]; }
            array = (E[]) newArray; }
    }

    public void addLast(E item) {
        if (array.length == 0) {
            array = (E[]) new Object[1];
            array[0] = item; }
        else {
            Object[] newItems = new Object[array.length + 1];
            newItems[newItems.length - 1] = item;
            for (int i = 0; i < newItems.length - 1; i++) {
                newItems[i] = array[i]; }
            array = (E[]) newItems; }
    }

    public boolean offer(E object) {
        add(object);
        return true;
    }

    public boolean offerFirst(E object) {
        addFirst(object);
        return true;
    }

    public boolean offerLast(E object) {
        addLast(object);
        return true;
    }

    public void push(E object) {
        if (array.length == 0) {
            array = (E[]) new Object[1];
            array[0] = object; }
        else {
            Object[] newArray = new Object[array.length + 1];
            newArray[0] = object;
            for (int i = 1; i < newArray.length; i++) {
                newArray[i] = array[i - 1]; }
            array = (E[]) newArray; }
    }
    public Object pop() {
        Object object = array[0];
        Object[] newArray = new Object[array.length - 1];
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = array[i + 1]; }
        array = (E[]) newArray;
        return object;
    }

    public Object removeLast() {
        Object object = array[array.length - 1];
        Object[] newArray = new Object[array.length - 1];
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = array[i]; }
        array = (E[]) newArray;
        return object;
    }

    public boolean contains(E object) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(object)) {
                return true;
            }
        }
        return false;
    }

    public void removeFirst(){
        pop();
    }

    public Object pollFirst() {
        return pop();
    }

    public Object pollLast() {
        return removeLast();
    }

    public Object getFirst() {
        return array[0];
    }

    public Object getLast() {
        return array[array.length - 1];
    }

    public Object peekFirst() {
        return getFirst();
    }

    public Object peekLast() {
        return getLast();
    }

}
