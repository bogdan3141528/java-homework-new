package priorityQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ship_with_droids.Droid;
import ship_with_droids.MainGenericClassShipWithDroid;
import ship_with_droids.SuperDroid;

public class MainPriorityQueue {
    private static Logger log = LogManager.getLogger(MainPriorityQueue.class);

    public static void main(String[] args) {
        Droid droid = new Droid("Droid1");
        SuperDroid superdroid = new SuperDroid("SuperDroid1");
        PriorityQueueForDroids queueRaw = new PriorityQueueForDroids();

        log.info("Raw type");
        queueRaw.add("Bohdan");
        log.info(queueRaw.contains("Bohdan"));
        queueRaw.removeFirst();
        log.info(queueRaw.contains("Bohdan"));
        queueRaw.add(droid);
        queueRaw.add(superdroid);
        log.info(queueRaw.contains(droid));
        log.info(queueRaw.contains(superdroid));

        log.info("Declaration type");
        PriorityQueueForDroids<Droid> queueDeclared = new PriorityQueueForDroids<Droid>();
        //queueDeclared.add("Bohdan");//not compile
        queueDeclared.add(droid);
        //queueDeclared.add(superdroid);//not compile
        log.info(queueDeclared.contains(droid));


    }
}
