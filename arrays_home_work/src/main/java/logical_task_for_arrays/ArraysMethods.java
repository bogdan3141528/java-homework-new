package logical_task_for_arrays;

import org.apache.commons.lang3.ArrayUtils;

public class ArraysMethods {

    int[] removeDuplicates(int[] array) {
        int end = array.length;
        for (int i = 0; i < end; i++) {
            for (int j = i + 1; j < end; j++) {
                if (array[i] == array[j]) {
                    array[j] = array[end - 1];
                    end--;
                    j--;
                }
            }
        }
        int[] newArray = new int[end];
        System.arraycopy(array, 0, newArray, 0, end);
        return newArray;
    }

    int[] findCommonElement(int[] arr1, int[] arr2) {
        int[] arr3 = new int[arr1.length + arr2.length];
        int end = 0;
        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] == arr2[j]) {
                    arr3[end++] = arr1[i];
                }
            }
        }
        int[] newArray = new int[end];
        System.arraycopy(arr3, 0, newArray, 0, end);
        return removeDuplicates(newArray);
    }

    int[] leftJoinElement(int[] arr1, int[] arr2) {

        for (int i = 0; i < arr1.length; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr1[i] == arr2[j]) {
                    arr1 = ArrayUtils.remove(arr1, i);
                }
            }
        }
        return arr1;
    }

    int[] removeDuplicates1RepeatedMoreThanTwice(int[] array) {
        int k = 0;
        for (int i = 0; i < array.length; i++) {
            k = 0;
            for (int j = 1; j < array.length - 1; j++) {
                if (array[i] == array[j]) {
                    k++;
                    if (k >= 2) {
                        array = ArrayUtils.remove(array, i);
                    }
                }
            }
        }
        return array;
    }

    int[] markerIdenticalElements(int[] array) {
        int k = 0;
        int[] arr3 = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            k = 0;
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    k++;
                }
            }
            if (k == 0) {
                arr3[i] = array[i];
            }
        }
        return arr3;
    }

    int[] removeSeriesOfIdenticalElements(int[] array) {
        int[] arr1 = new int[array.length];
        int end = 0;
        array = markerIdenticalElements(array);
        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                arr1[end++] = array[i];
            }
        }
        int[] newArray = new int[end];
        System.arraycopy(arr1, 0, newArray, 0, end);
        return newArray;
    }

}

