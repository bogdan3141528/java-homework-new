package logical_task_for_arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class MainClassArraysMethods {
    private static Logger logger = LogManager.getLogger(MainClassArraysMethods.class);

    public static void main(String[] args) {

        logger.info("Create instance to test");
        ArraysMethods arrays = new ArraysMethods();
        logger.info("Create array to test removing duplicates");
        int[] arr = {11, 24, 10, 5, 3, 2, 11 ,3, 10, 1, 6, 7, 5, 7, 8, 2, 2, 3, 1, 15, 24, 10, 8};
        logger.trace("Before removing: "+ Arrays.toString(arr));
        logger.info("Delete  duplicates: "+
                Arrays.toString(arrays.removeDuplicates(arr)));
        logger.info("Create 2 arrays to test join of them");
        int [] arr1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 200, 5, 6, 5 };
        logger.trace("Before removing: "+Arrays.toString(arr1));
        int [] arr2 = { 12, 2, 3, 2, 200, 5 };
        logger.trace("Before removing: "+Arrays.toString(arr2));
        logger.info("Elements exist in Both Arrays: "+
                Arrays.toString(arrays.findCommonElement(arr1, arr2)));
        logger.info("Elements exist one array: "+
                Arrays.toString(arrays.leftJoinElement(arr1, arr2)));
        logger.info("Create array to test removing equal elements in a row with leaving one ");
        int[] arr3 = {1, 2, 2, 2, 2, 2, 3, 4, 5, 6, 6, 6, 7, 7,};
        logger.trace("Before removing: "+Arrays.toString(arr3));
        logger.info("Groups of elements detected and reduced to 1 element in each group: "
                +Arrays.toString(arrays.removeDuplicates1RepeatedMoreThanTwice(arr3)));
        logger.info("Create array to test removing equal elements in a row with leaving one ");
        int[] arr4 = {1, 2, 2, 2, 2, 2, 3, 4, 5, 6, 6, 6, 7, 7, 7, 8,};
        logger.trace("Before removing: "+Arrays.toString(arr4));
        logger.info("Groups of elements detected and reduced to 1 element in each group: "
                +Arrays.toString(arrays.removeSeriesOfIdenticalElements(arr4)));
    }
}
