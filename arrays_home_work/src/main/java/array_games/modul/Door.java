package array_games.modul;

import java.util.Random;

public class Door {
    public boolean isMonster;
    public Monster monster;
    public Artifact artifact;
    Random rd = new Random();

    public boolean genWhoIsBehindTheDoor() {
        isMonster = rd.nextBoolean();
        if (isMonster) {
            monster = new Monster();
            return true; }
        else {
            artifact = new Artifact();
            return false; }
    }
}
