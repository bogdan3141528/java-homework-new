package array_games.modul;

import java.util.Random;

public class Artifact {
    private static final int MIN_FORCE_VALUE = 10;
    private static final int MAX_FORCE_VALUE = 80;
    private int random;

    public int genRandom() {
        Random r = new Random();
        random =  r.nextInt((MAX_FORCE_VALUE - MIN_FORCE_VALUE) + 1)+MIN_FORCE_VALUE;
        return random;
    }
}
