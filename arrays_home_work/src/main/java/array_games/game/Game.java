package array_games.game;

import array_games.modul.Door;
import array_games.modul.Hero;
import logical_task_for_arrays.MainClassArraysMethods;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Game {

    private static Logger logger = LogManager.getLogger(MainClassArraysMethods.class);
    private Hero h = new Hero();
    private Door[] doors;
    private String result;
    private int[] powerMonster = new int[11];
    private int[] powerArtifact = new int[11];
    private int heroPower=h.getTHE_STRENGTH_OF_THE_HERO();

    public Game(){
        getDoors();
        winning();
        death();
        System.out.println(winingRecursive(10)); //recursive method
        howToWinTheGame();
        System.out.println(heroPower);
    }

    private void getDoors() {
        doors = new Door[11];
        for (int i = 1; i < doors.length; i++) {
            doors[i] = new Door(); }
        for (int i = 1; i < doors.length; i++) {
            if (doors[i].genWhoIsBehindTheDoor()) {
                powerMonster[i] =doors[i].monster.genRandom();
                logger.warn("behind door number "+ i +" Monster "+ powerMonster[i]); }
            else {
                powerArtifact[i] =doors[i].artifact.genRandom();
                logger.info("behind door number "+ i +" Artifact"+ powerArtifact[i]); } }
    }

    private void winning(){
        for (int i = 1; i < doors.length; i++) {
            if (doors[i].isMonster && (powerMonster[i]<=25)){
                logger.info("We will win when we enter door number: "+i); } }
    }

    private String winingRecursive(int i) {
        if (i == 0) {
            return result; }
        if (doors[i].isMonster && (powerMonster[i] <= 25)) {
            result = ("We will win when we enter door number: " + i); }
       return winingRecursive(i - 1);
    }

    private void death(){
        for (int i = 1; i < doors.length; i++) {
            if (doors[i].isMonster && (powerMonster[i]>25)){
                logger.info("We will let's die when we enter door number: "+i); } }
    }

    private void openArtifactDoors(){
        for (int i = 1; i <doors.length ; i++) {
            if (!doors[i].isMonster){
                logger.info("We open the door number: "+i);
                heroPower= powerArtifact[i]+heroPower;
                logger.info("Now the strength of the hero: "+heroPower); } }
        if (heroPower>=100) {
            logger.trace("Then we can open any door and win the game"); }
    }
    private void openMonsterDoors() {
        for (int i = 1; i < doors.length; i++){
            if (doors[i].isMonster &&(heroPower >= powerMonster[i])) {
                logger.warn("We will win when we enter door number: " + i); }
            if  (doors[i].isMonster &&(heroPower < powerMonster[i])){
                logger.error("We can not win this games");} }
    }

    private void howToWinTheGame(){
        openArtifactDoors();
        openMonsterDoors();
    }
}
