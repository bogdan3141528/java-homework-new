package array_colections.string_container;

import logical_task_for_arrays.MainClassArraysMethods;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class StringCollections {

    private static Logger logger = LogManager.getLogger(StringCollections.class);
    private Object[] array;

    StringCollections() {
        array = new String[0];
    }

    public void add(Object o) {
        if(!(o instanceof String)){
            logger.error("We can add only String in StringCollections");
        }
        else if (array.length == 0) {
            array = new String[1];
            array[0] = o; }
        else {
            String[] arrNew = new String[array.length + 1];
            arrNew[0] = (String) o;
            for (int i = 1; i < arrNew.length; i++) {
                arrNew[i] = (String) array[i - 1]; }
            array = arrNew;
            logger.info("Element: "+o+" add!!!"); }
    }

    boolean contains(Object o){
        boolean flag = true;
        if(!(o instanceof String)){
            return false; }
        else {
            for (int i = 0; i < array.length; i++) {
                if(array[i].equals(o)){
                    return true; }
                else {
                    flag= false; } }
        return flag;}
    }

    public void remove(Object o) {
        if(!(o instanceof String)){
            logger.error("StringCollections can contains only String"); }
        else if (contains(o)){
            for (int i = 0; i < array.length; i++) {
                if (array[i].equals(o)) {
                    array = ArrayUtils.removeElement(array, array[i]);
                    logger.info("Element: "+o+" remove!!!"); } } }
        else {
            logger.info("Element: "+o+" is not in StringCollections !!!"); } }

}
