package array_colections.string_container;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class MainStringCollections {
    public static void main(String[] args) {

        Logger logger = LogManager.getLogger(MainStringCollections.class);
        Random r = new Random();
        String random;

        StringCollections stringCollections = new StringCollections();
        for (int i = 0; i < 10; i++) {
            random =  String.valueOf((r.nextInt((100 - 0) + 1)));
            stringCollections.add(random);
        }
        stringCollections.add(12);
        logger.info(stringCollections.contains(12));
        stringCollections.add("Bohdan");
        logger.info(stringCollections.contains("Bohdan"));
        stringCollections.remove("Bohdan");
        logger.info(stringCollections.contains("Bohdan"));


    }
}
