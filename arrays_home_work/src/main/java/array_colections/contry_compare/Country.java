package array_colections.contry_compare;

public class Country{
    private String countryName;
    private String capitalName;

    Country(String country, String capital) {
        this.countryName = country;
        this.capitalName = capital;
    }

    String getCountryName() {
        return countryName; }

    String getCapitalName() {
        return capitalName; }

    @Override
    public String toString() {
        return "countryName='" + countryName + '\'' +
                ", capital='" + capitalName + '\'';
    }
}
