package array_colections.contry_compare;

public class CompereCapitalName implements Comparable<CompereCapitalName> {

    private Country country;

    public CompereCapitalName(Country country) {
        this.country=country;
    }
    @Override
    public String toString() {
       return "country=" + country;
    }
    @Override
    public int compareTo(CompereCapitalName o) {
        return this.country.getCapitalName().compareTo(country.getCapitalName());
    }
}
