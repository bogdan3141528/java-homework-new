package array_colections.contry_compare;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;


public class MainCompareCountry {
    public static void main(String[] args) {

        Logger logger = LogManager.getLogger(MainCompareCountry.class);

    Country[] country = new Country[]{
            new Country("Ukraine", "Kiev"),
            new Country("Poland", "Warsaw"),
            new Country("Germany", "Berlin"),
            new Country("Hungary", "Budapest"),
            new Country("Latvia", "Riga"),
            new Country("USA", "Washington"),
            new Country("Australia", "Canberra"),
            new Country("Estonia", "Talin"),
    };

    List<CompereCountryName> countryNames = new ArrayList<CompereCountryName>();
    List<CompereCapitalName> capitalNames = new ArrayList<CompereCapitalName>();

       for (int i = 0; i < country.length; i++) {
        countryNames.add(new CompereCountryName(country[i]));
        capitalNames.add(new CompereCapitalName(country[i]));
    }
       countryNames.sort(CompereCountryName ::compareTo);
       capitalNames.sort(CompereCapitalName ::compareTo);

        for (int i = 0; i <country.length ; i++) {
            logger.info(countryNames.get(i));
            logger.info(capitalNames.get(i));
        }
        countryNames.forEach(compereCountryName -> logger.info(countryNames));


    }
}
