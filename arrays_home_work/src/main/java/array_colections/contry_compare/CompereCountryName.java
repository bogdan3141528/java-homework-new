package array_colections.contry_compare;

public class CompereCountryName implements Comparable<CompereCountryName> {

    private Country country;

    public CompereCountryName(Country country) {
        this.country=country;
    }
    @Override
    public String toString() {
        return "country=" + country;

    }

    @Override
    public int compareTo(CompereCountryName o) {
        return this.country.getCountryName().compareTo(country.getCountryName());
    }
}
