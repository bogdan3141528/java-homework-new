package array_colections.deque;

import com.sun.org.apache.xpath.internal.axes.DescendantIterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyDeque implements Iterable {

    private Object[] array;

    MyDeque() {
        array = new Object[0];
    }

   /* add(element): Adds an element to the tail.
    addFirst(element): Adds an element to the head.
    addLast(element): Adds an element to the tail.
    offer(element): Adds an element to the tail and returns a boolean to explain if the insertion was successful.
    offerFirst(element): Adds an element to the head and returns a boolean to explain if the insertion was successful.
    offerLast(element): Adds an element to the tail and returns a boolean to explain if the insertion was successful.
    iterator(): Returna an iterator for this deque.
            descendingIterator(): Returns an iterator that has the reverse order for this deque.
            push(element): Adds an element to the head.
    pop(element): Removes an element from the head and returns it.
            removeFirst(): Removes the element at the head.
    removeLast(): Removes the element at the tail.
    poll(): Retrieves and removes the head of the queue represented by this deque (in other words, the first element of this deque), or returns null if this deque is empty.
            pollFirst(): Retrieves and removes the first element of this deque, or returns null if this deque is empty.
            pollLast(): Retrieves and removes the last element of this deque, or returns null if this deque is empty.
            peek(): Retrieves, but does not remove, the head of the queue represented by this deque (in other words, the first element of this deque), or returns null if this deque is empty.
            peekFirst(): Retrieves, but does not remove, the first element of this deque, or returns null if this deque is empty.
            peekLast(): Retrieves, but does not remove, the last element of this deque, or returns null if this deque is empty.*/

   public void add(Object o) {
       if (array.length == 0) {
           array = new Object[1];
           array[0] = o; }
       else {
           Object[] newItems = new Object[array.length + 1];
           newItems[newItems.length - 1] = o;
           for (int i = 0; i < newItems.length - 1; i++) {
               newItems[i] = array[i]; }
           array = newItems; }
   }

   public void addFirst(Object o) {
        if (array.length == 0) {
            array = new Object[1];
            array[0] = o; }
        else {
            Object[] newArray = new Object[array.length + 1];
            newArray[0] = o;
            for (int i = 1; i < newArray.length; i++) {
                newArray[i] = array[i - 1]; }
            array = newArray; }
    }

    public void addLast(Object o) {
        if (array.length == 0) {
            array = new Object[1];
            array[0] = o; }
        else {
            Object[] newItems = new Object[array.length + 1];
            newItems[newItems.length - 1] = o;
            for (int i = 0; i < newItems.length - 1; i++) {
                newItems[i] = array[i]; }
            array = newItems; }
    }

    public boolean offer(Object o) {
            add(o);
            return true;
    }

    public boolean offerFirst(Object o) {
            addFirst(o);
            return true;
    }

    public boolean offerLast(Object o) {
            addLast(o);
            return true;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    public DescendantIterator descendingIterator (){
        return null;
    }
    public void push(Object o) {
        if (array.length == 0) {
            array = new Object[1];
            array[0] = o; }
        else {
            Object[] newArray = new Object[array.length + 1];
            newArray[0] = o;
            for (int i = 1; i < newArray.length; i++) {
                newArray[i] = array[i - 1]; }
            array = newArray; }
    }
    public Object pop() {
        Object object = array[0];
        Object[] newArray = new Object[array.length - 1];
            for (int i = 0; i < newArray.length; i++) {
                newArray[i] = array[i + 1]; }
            array = newArray;
            return object;
   }

    public Object removeLast() {
        Object object = array[array.length - 1];
        Object[] newArray = new Object[array.length - 1];
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = array[i]; }
        array = newArray;
        return object;
   }

   public void removeFirst(){
       pop();
   }

    public Object pollFirst() {
            return pop();
    }

    public Object pollLast() {
            return removeLast();
    }

    public Object getFirst() {
            return array[0];
    }

    public Object getLast() {
            return array[array.length - 1];
    }

    public Object peekFirst() {
            return getFirst();
    }

    public Object peekLast() {
            return getLast();
    }

}
