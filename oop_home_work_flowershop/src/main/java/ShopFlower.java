import java.util.Arrays;

public class ShopFlower implements Comparable {


    public int price;
    public String name;

    public ShopFlower(String name, int price) {
        this.price = price;
        this.name = name;

    }

    public static void main(String[] args) {
        Flowers[] arr = new Flowers[]{new Chamomiles(), new LiliesOfTheValley(), new RedRoses(),
                new WhiteRoses(),};
        for (int i = 0; i < arr.length; i++) {
            arr[i].priceСalculation(5);
            System.out.println("=====================");
        }


        for (int i = 0; i < arr.length; i++) {
            String[] anArray = new String[4];
            anArray[i] = arr[i].priceSum(5);
            System.out.println(anArray[i]);
        }
        System.out.println("=====================");

        Chamomiles c = new Chamomiles();
        c.priceSum(5);
        LiliesOfTheValley l = new LiliesOfTheValley();
        l.priceSum(5);
        RedRoses r = new RedRoses();
        r.priceSum(5);
        WhiteRoses w = new WhiteRoses();
        w.priceSum(5);


        ShopFlower[] trysortarray = new ShopFlower[4];
        trysortarray[0] = new ShopFlower(c.getName(), c.sum);
        trysortarray[1] = new ShopFlower(l.getName(), l.sum);
        trysortarray[2] = new ShopFlower(r.getName(), r.sum);
        trysortarray[3] = new ShopFlower(w.getName(), w.sum);

        Arrays.sort(trysortarray);
        System.out.println("Price sort!!!!!!!!!");

        for (int i = 0; i < trysortarray.length; i++) {
            System.out.println(trysortarray[i].price +"  "+
                    trysortarray[i].name);
        }
    }

    public int compareTo(Object o) {
        ShopFlower tmp = (ShopFlower) o;
        if (this.price < tmp.price) {
            return -1;
        } else if (this.price > tmp.price) {
            return 1;
        }
        return 0;
    }

}

