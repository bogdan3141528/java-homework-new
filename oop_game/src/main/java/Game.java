
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;
import java.util.Scanner;

public class Game extends JPanel implements ActionListener {
    private final int SIZE = 320;
    private final int DOT_SIZE = 16;
    private final int ALL_DOTS = 400;
    private Image dot;
    private Image apple;
    private int appleX;
    private int appleY;
    private int[] x = new int[ALL_DOTS];
    private int[] y = new int[ALL_DOTS];
    private int dots;
    private Timer timer;
    private boolean left = false;
    private boolean right = true;
    private boolean up = false;
    private boolean down = false;
    private boolean inGame = true;
    private String str;
    private String picture_A;
    private String picture_B;
    private int speed;


    void  choise() {
        Scanner n = new Scanner(System.in);
        String ch = n.nextLine();

        if (ch.equals("Fish")) {
            Fish f = new Fish();
            str = f.eat_anything("WORMS");
            f.sleep(0);
            f.swim();
            speed = f.getSpeed(300);
            picture_A="/home/bohdan/Learn(Python&Java)/Java/IdeaProjects/java_home_work/oop_game/A.png";
            picture_B="/home/bohdan/Learn(Python&Java)/Java/IdeaProjects/java_home_work/oop_game/B.png";

        }

        else if (ch.equals("Bird")) {
            Bird b = new Bird();
            str = b.eat_anything("worms and something eals");
            b.sleep(6);
            speed = b.getSpeed(100);
            b.fly();
            picture_A="/home/bohdan/Learn(Python&Java)/Java/IdeaProjects/java_home_work/oop_game/A.png";
            picture_B="/home/bohdan/Learn(Python&Java)/Java/IdeaProjects/java_home_work/oop_game/B1.png";
        }
        else{
            System.out.println("Ви ввели не правельно!");
            str = "Error!!!";

        }
    }



    public Game() {
        choise();
        setBackground(Color.black);
        loadimages();
        initGame();
        addKeyListener(new Key());
        setFocusable(true);
    }

    public void initGame() {
        dots = 3;
        for (int i = 0; i < dots; i++) {
            x[i] = 48 - i * DOT_SIZE;
            y[i] = 48;
        }
        timer = new Timer(speed, this);
        timer.start();
        createApple();

    }

    public void createApple() {
        appleX = new Random().nextInt(20) * DOT_SIZE;
        appleY = new Random().nextInt(20) * DOT_SIZE;
    }

    public void loadimages() {
        ImageIcon iia = new ImageIcon(picture_A);
        apple = iia.getImage();
        ImageIcon iid = new ImageIcon(picture_B);
        dot = iid.getImage();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (inGame) {
            g.drawImage(apple, appleX, appleY, this);
            for (int i = 0; i < dots; i++) {
                g.drawImage(dot, x[i], y[i], this);

            }
        }
            else {
                g.setColor(Color.white);
                g.drawString(str,125,SIZE/2);
            }

    }

    public void move() {
        for (int i = dots; i > 0; i--) {
            x[i] = x[i - 1];
            y[i] = y[i - 1];
        }
        if (left) {
            x[0] -= DOT_SIZE;
        }
        if (right) {
            x[0] += DOT_SIZE;
        }
        if (up) {
            y[0] -= DOT_SIZE;
        }
        if (down) {
            y[0] += DOT_SIZE;
        }
    }

    public void checkapple() {
        if (x[0] == appleX && y[0] == appleY) {
            dots++;
            createApple();
        }
    }

    public void checkColision() {
        for (int i = dots; i > 0; i--) {
            if (i > 4 && x[0] == x[i] && y[0] == y[i]) {
                inGame = false;

            }
        }
        if (x[0] > SIZE) {
            inGame = false;
        }
        if (x[0] < 0) {
            inGame = false;
        }
        if (y[0] > SIZE) {
            inGame = false;
        }
        if (y[0] < 0) {
            inGame = false;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (inGame) {
            checkapple();
            checkColision();
            move();
        }
        repaint();
    }

    class Key extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            super.keyPressed(e);
            int key = e.getKeyCode();
            if ( key == KeyEvent.VK_LEFT && !right) {
                left = true;
                up = false;
                down = false;
            }

            if (key == KeyEvent.VK_RIGHT && !left) {
                right = true;
                up = false;
                down = false;
            }
            if (key == KeyEvent.VK_UP && !down) {
                left = false;
                up = true;
                right = false;
            }

            if (key == KeyEvent.VK_DOWN && !up) {
                left = false;
                down = true;
                right = false;
            }


        }
    }
}