
import javax.swing.*;

public class MainWindow extends JFrame {

    public MainWindow() {
        setTitle("Animal");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(350, 350);
        setLocation(400, 400);
        add(new Game());
        setVisible(true);
    }

    public static void main(String[] args) {
        System.out.println("Введіть з клавіатури за кого грати Fish or Bird");
        MainWindow mw = new MainWindow();
    }
}
